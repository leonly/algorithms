///quik_sort average O(nlog(n))
pub fn sort<T>(arr: &mut [T])
where
    T: PartialEq + PartialOrd,
{
    if arr.len() <= 1 {
        return;
    }
    _quik_sort(arr, 0, arr.len() - 1);
}

///快速排序的优化：三中值和排序小数组（len<5）采用插入排序
fn _quik_sort<T: PartialOrd + PartialEq>(arr: &mut [T], lo: usize, hi: usize) {
    if lo < hi {
        let p = partition(arr, lo, hi);
        _quik_sort(arr, lo, p - 1);
        _quik_sort(arr, p + 1, hi);
    }
}

fn partition<T>(arr: &mut [T], lo: usize, hi: usize) -> usize
where
    T: PartialEq + PartialOrd,
{
    let pivot = hi;
    let mut i = lo;
    for j in lo..pivot {
        if arr[j] < arr[pivot] {
            arr.swap(i, j);
            i += 1;
        }
    }
    arr.swap(i, pivot);
    i
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sort() {
        let mut arr = [1, 4, 3, 2, 2, 3, 3, 3, 5];
        sort(&mut arr);
        assert_eq!(arr, [1, 2, 2, 3, 3, 3, 3, 4, 5])
    }
}
