use std::cmp;

/// 插入排序，从前向后遍历已有序vec，适合原始数据本来就有较好的逆序顺序排序，O(n^2)
pub fn sort<T>(arr: &[T]) -> Vec<T>
where
    T: cmp::PartialEq + cmp::PartialOrd + Clone,
{
    let mut result: Vec<T> = Vec::with_capacity(arr.len());

    for elem in arr.iter().cloned() {
        let n_inserted = result.len();
        for i in 0..=n_inserted {
            if i == n_inserted || result[i] > elem {
                result.insert(i, elem);
                break;
            }
        }
    }
    result
}

/// 插入排序，从后向前遍历已有序vec，适合原始数据本来就有较好的顺序顺序排序，O(n^2)
pub fn sort_i<T>(arr: &[T]) -> Vec<T>
where
    T: cmp::PartialEq + cmp::PartialOrd + Clone,
{
    let mut result: Vec<T> = Vec::with_capacity(arr.len());

    for elem in arr.iter().cloned() {
        let n_inserted = result.len();
        for i in 0..=n_inserted {
            if i == n_inserted || result[n_inserted - i - 1] <= elem {
                result.insert(n_inserted - i, elem);
                break;
            }
        }
    }
    result
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sort_test() {
        let arr = [1, 32, 2, 1, 3, 3, 3, 3];
        let re = sort(&arr);
        let re1 = sort_i(&arr);
        assert_eq!(re, vec![1, 1, 2, 3, 3, 3, 3, 32]);
        assert_eq!(re1, vec![1, 1, 2, 3, 3, 3, 3, 32]);
    }
}
