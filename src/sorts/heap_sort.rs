/// 堆排序, 是一个不稳定的排序，因为它频繁移动元素，所以尽量避免基于值的排序。
/// 堆排序避免了快排很多导致性能退化的情况，尽管如此，平均情况下没有快排效率高
/// O(nlog(n))
pub fn sort<T: Ord>(arr: &mut [T]) {
    // 构建堆树，使得最大数被抬升至第一位
    build_heap(arr);
    for i in (1..arr.len()).rev() {
        arr.swap(0, i);
        heapify(arr, 0, i);
    }
}

fn build_heap<T: Ord>(arr: &mut [T]) {
    let l = (arr.len() + 1) / 2;
    for i in (0..l).rev() {
        heapify(arr, i, arr.len());
    }
}

/// 堆排序中生成堆的算法，，一个堆就是一个二叉树，节点从左到右添加，树种每个节点的值大于等于任意
/// 一个子节点的值，递归调用heapify使得二叉树满足上述性质，最终大数会被“抬升”
fn heapify<T: Ord>(arr: &mut [T], idx: usize, max: usize) {
    let left = idx * 2 + 1;
    let right = idx * 2 + 2;
    let mut largest = idx;
    if left < max && arr[left] > arr[largest] {
        largest = left;
    }
    if right < max && arr[right] > arr[largest] {
        largest = right;
    }
    if largest != idx {
        arr.swap(idx, largest);
        heapify(arr, largest, max);
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sort() {
        let mut arr = [5, 3, 16, 2, 10, 14];
        sort(&mut arr);
        assert_eq!(arr, [2, 3, 5, 10, 14, 16]);
    }
}
