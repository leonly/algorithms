pub fn merge_sort<'a, T: Ord + Copy>(arr1: &'a mut [T], arr2: &'a mut [T]) -> Vec<T> {
    let mut result: Vec<T> = Vec::with_capacity(arr1.len() + arr2.len());

    //声明两个指针指向两个数组
    let mut a1 = 0;
    let mut a2 = 0;

    while a1 < arr1.len() && a2 < arr2.len() {
        if arr1[a1] < arr2[a2] {
            result.push(arr1[a1]);
            a1 += 1;
        } else {
            result.push(arr2[a2]);
            a2 += 1;
        }
    }
    while a1 < arr1.len() {
        result.push(arr1[a1]);
        a1 += 1;
    }
    while a2 < arr2.len() {
        result.push(arr2[a2]);
        a2 += 1;
    }
    result
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_merge_sort() {
        let mut arr1 = [1, 2, 3, 4, 5];
        let mut arr2 = [3, 5, 7, 8, 9];
        let re = merge_sort(&mut arr1, &mut arr2);
        assert_eq!(re, vec![1, 2, 3, 3, 4, 5, 5, 7, 8, 9]);
    }
}
