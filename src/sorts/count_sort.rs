/// 计数排序，不需要比较函数（基于比较的排序最高性能nlog(n)）,是对范围
/// 固定在·[0,k)的整数排序的最佳选择
///  O(n)
pub fn sort(arr: &mut [usize], max_value: usize) {
    let mut occurences: Vec<usize> = vec![0; max_value + 1];
    for &data in arr.iter() {
        occurences[data] += 1;
    }
    let mut i = 0;
    for (index, &item) in occurences.iter().enumerate() {
        for _ in 0..item {
            arr[i] = index;
            i += 1;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sort_test() {
        let mut arr = [1, 4, 3, 3, 4, 5, 5, 33];
        sort(&mut arr, 33);
        assert_eq!(arr, [1, 3, 3, 4, 4, 5, 5, 33]);
    }
}
