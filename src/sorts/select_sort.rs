pub fn sort<T: Ord>(arr: &mut [T]) {
    let mut smallest: usize = 0;
    for i in 0..arr.len() - 1 {
        for j in i + 1..arr.len() {
            if arr[j] < arr[smallest] {
                smallest = j;
            }
        }
        arr.swap(i, smallest)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_sort() {
        let mut arr = [8];
        sort(&mut arr);
        assert_eq!(arr, [8]);
        let mut arr = [8, 9, 0, 4, 4, 3];
        sort(&mut arr);
        assert_eq!(arr, [0, 3, 4, 4, 8, 9]);
    }
}
