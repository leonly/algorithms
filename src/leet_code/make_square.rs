struct Solution;

impl Solution {
    pub fn makesquare(nums: Vec<i32>) -> bool {
        let perimeter = nums.iter().fold(0, |acc, x| acc + x);
        let (mut a1, mut a2, mut a3) = (0, 0, 0);
        if perimeter % 4 != 0 || nums.len() < 4 {
            return false;
        }
        let lenth = perimeter / 4;
        let mut nums = nums;
        nums.sort();
        nums.reverse();
        for num in nums.iter() {
            if a1 + num <= lenth {
                a1 += num;
            } else if a2 + num <= lenth {
                a2 += num;
            } else if a3 + num <= lenth {
                a3 += num;
            }
        }
        if a1 == lenth && a2 == lenth && a3 == lenth {
            return true;
        }
        a1 = 0;
        a2 = 0;
        a3 = 0;
        for num in nums.iter() {
            if a1 + num <= lenth {
                if a1 == 0 || a1 + num == lenth {
                    a1 += num;
                    continue;
                } else {
                    let left = lenth - a1 - num;
                    if nums.contains(&left) || left >= lenth/4 {
                        a1 += num;
                        continue;
                    }
                }
            }
            if a2 + num <= lenth {
                if a2 == 0 || a2 + num == lenth {
                    a2 += num;
                    continue;
                } else {
                    let left = lenth - a2 - num;
                    if nums.contains(&left) || left >= lenth/4 {
                        a2 += num;
                        continue;
                    }
                }
            }
            if a3 + num <= lenth {
                if a3 == 0 || a3 + num == lenth {
                    a3 += num;
                    continue;
                } else {
                    let left = lenth - a3 - num;
                    if nums.contains(&left) || left >= lenth/4 {
                        a3 += num;
                        continue;
                    }
                }
            }
        }
        if a1 == lenth && a2 == lenth && a3 == lenth {
            return true;
        }
        false
    }
}
