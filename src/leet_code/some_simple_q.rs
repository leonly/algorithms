use std::cmp;
use std::collections::HashSet;
use std::str::FromStr;

struct Solution;

struct MinStack {
    stack: Vec<i32>,
    min_stack: Vec<i32>,
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

impl Solution {
    // 反转一个整数
    pub fn reverse(x: i32) -> i32 {
        if x > -10 && x < 10 {
            return x;
        }
        let (flag, x) = if x < 0 { (false, -x) } else { (true, x) };
        let rev: String = x.to_string().chars().rev().collect();
        let re = i32::from_str(&rev);
        match re {
            Ok(num) => {
                if !flag {
                    -num
                } else {
                    num
                }
            }
            Err(_) => 0,
        }
    }

    pub fn kids_with_candies(candies: Vec<i32>, extra_candies: i32) -> Vec<bool> {
        let mut res: Vec<bool> = Vec::with_capacity(candies.len());
        let mut max_num = 0;
        for &item in candies.iter() {
            if item > max_num {
                max_num = item;
            }
        }
        for &item in candies.iter() {
            if item + extra_candies >= max_num {
                res.push(true);
            } else {
                res.push(false);
            }
        }
        res
    }

    pub fn max_diff(num: i32) -> i32 {
        let num_str = num.to_string();
        let mut rep_one: String = String::from("*");
        let mut rep_two: String = String::from("*");

        let mut rep_first = false;
        for c in num_str.chars() {
            if c != '9' {
                rep_one = c.to_string();
                break;
            }
        }
        let mut k = 0;
        for c in num_str.chars() {
            if c != '1' && k == 0 {
                rep_two = c.to_string();
                rep_first = true;
                break;
            } else if c != '0' && c != '1' {
                rep_two = c.to_string();
                break;
            }
            k += 1;
        }

        let rp_num1: i32 = num_str.replace(&rep_one, "9").parse().unwrap();
        let rp_num2: i32 = if rep_first {
            num_str.replace(&rep_two, "1").parse().unwrap()
        } else {
            num_str.replace(&rep_two, "0").parse().unwrap()
        };
        rp_num1 - rp_num2
    }
}

/**
 * `&self` means the method takes an immutable reference.
 * If you need a mutable reference, change it to `&mut self` instead.
 */
impl MinStack {
    /** initialize your data structure here. */
    fn new() -> Self {
        MinStack {
            stack: Vec::new(),
            min_stack: Vec::new(),
        }
    }

    fn push(&mut self, x: i32) {
        self.stack.push(x);
        if self.min_stack.len() > 0 {
            let min = self.min_stack.get(self.min_stack.len() - 1).unwrap();
            self.min_stack.push(cmp::min(x, *min));
        } else {
            self.min_stack.push(x);
        }
    }

    fn pop(&mut self) {
        self.min_stack.pop();
        self.stack.pop();
    }

    fn top(&self) -> i32 {
        *self.stack.get(self.stack.len() - 1).unwrap()
    }

    fn get_min(&self) -> i32 {
        *self.min_stack.get(self.min_stack.len() - 1).unwrap()
    }

    // 判断一个整数是否是回文数。回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
    pub fn is_palindrome(x: i32) -> bool {
        let s: String = x.to_string();
        s == s.chars().rev().collect::<String>()
    }

    // 给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
    // 你的算法应该具有线性时间复杂度。 你可以不使用额外空间来实现吗？
    pub fn single_number(nums: Vec<i32>) -> i32 {
        let mut sigle: i32 = 0;
        for num in nums {
            sigle ^= num;
        }
        sigle
    }

    // 26. 删除排序数组中的重复项
    pub fn remove_duplicates(nums: &mut Vec<i32>) -> i32 {
        if nums.len() < 2 {
            return nums.len() as i32;
        }
        let mut last: i32 = nums[0];
        let mut index: usize = 1;
        for _ in 1..nums.len() {
            if nums[index] == last {
                nums.remove(index);
            } else {
                last = nums[index];
                index += 1;
            }
        }
        nums.len() as i32
    }

    // 26. 删除排序数组中的重复项
    pub fn remove_duplicates2(nums: &mut Vec<i32>) -> i32 {
        if nums.len() < 2 {
            return nums.len() as i32;
        }
        let mut last: i32 = nums[0];
        let mut index_replace: usize = 1;
        for i in 1..nums.len() {
            if nums[i] != last {
                last = nums[i];
                if i != index_replace {
                    nums[index_replace] = last;
                }
                index_replace += 1;
            }
        }
        nums.truncate(index_replace);
        index_replace as i32
    }

    // 27. 移除元素
    pub fn remove_element(nums: &mut Vec<i32>, val: i32) -> i32 {
        let mut result = nums.len();
        let mut k = 0;
        for _ in 0..nums.len() {
            if nums[k] == val {
                nums[k] = nums[result - 1];
                result -= 1;
            } else {
                k += 1;
            }
            if k >= result {
                break;
            }
        }
        result as i32
    }

    // 28. 实现 strStr()
    pub fn str_str(haystack: String, needle: String) -> i32 {
        let mut result = 0;
        let needle_len = needle.len();
        let haystack_len = haystack.len();
        if needle_len == 0 {
            return 0;
        }
        if needle_len > haystack.len() {
            return -1;
        }
        let start = needle.chars().nth(0).unwrap();
        for c in haystack.chars() {
            if c == start && &haystack[result..(result + needle_len)] == &needle {
                return result as i32;
            } else {
                result += 1;
            }
            if haystack_len - result < needle_len {
                break;
            }
        }
        -1
    }

    // 35. 搜索插入位置
    pub fn search_insert(nums: Vec<i32>, target: i32) -> i32 {
        let (mut left_index, mut right_index) = (0 as i32, nums.len() as i32 - 1);
        while left_index <= right_index {
            let mid = (left_index + right_index) / 2;
            if nums[mid as usize] == target {
                return mid;
            } else if nums[mid as usize] > target {
                right_index = mid - 1;
            } else {
                left_index = mid + 1;
            }
        }
        left_index
    }

    // 14. 最长公共前缀
    pub fn longest_common_prefix(strs: Vec<String>) -> String {
        let mut prefix = String::new();
        if strs.len() == 0 {
            return prefix;
        }
        'a: for c in strs[0].chars() {
            prefix.push(c);
            for i in 1..strs.len() {
                if !strs[i].starts_with(&prefix) {
                    prefix.pop();
                    break 'a;
                }
            }
        }
        prefix
    }

    // 20. 有效的括号
    pub fn is_valid(s: String) -> bool {
        let mut re: String = String::new();
        for c in s.chars() {
            if c == '(' || c == '[' || c == '{' {
                re.push(c);
            } else {
                let t = re.pop().unwrap_or('B');
                match c {
                    ')' => {
                        if t != '(' {
                            re.push(c);
                            break;
                        }
                    }
                    ']' => {
                        if t != '[' {
                            re.push(c);
                            break;
                        }
                    }
                    '}' => {
                        if t != '{' {
                            re.push(c);
                            break;
                        }
                    }
                    _ => break,
                }
            }
        }
        re.is_empty()
    }
    // 最大子序和
    pub fn max_sub_array(nums: Vec<i32>) -> i32 {
        let mut max_sub_sum = nums[0];
        let mut sub_sum = max_sub_sum;
        for i in 1..nums.len() {
            sub_sum += nums[i];
            if nums[i] > sub_sum {
                sub_sum = nums[i];
            }
            if sub_sum > max_sub_sum {
                max_sub_sum = sub_sum;
            }
        }
        max_sub_sum
    }

    // 给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。
    pub fn plus_one(digits: Vec<i32>) -> Vec<i32> {
        let mut digits = digits;
        let mut added = 1;
        for i in (0..digits.len()).rev() {
            digits[i] = if digits[i] + added > 9 {
                0
            } else {
                added = 0;
                digits[i] + 1
            };
            if added < 1 {
                break;
            }
        }
        if added > 0 {
            digits.insert(0, 1);
        }
        digits
    }
    //二进制求和
    pub fn add_binary(a: String, b: String) -> String {
        format!(
            "{:b}",
            i128::from_str_radix(a.as_str(), 2).unwrap()
                + i128::from_str_radix(b.as_str(), 2).unwrap()
        )
    }
    // 假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
    // 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
    pub fn climb_stairs(n: i32) -> i32 {
        let (mut p, mut q, mut r) = (0, 0, 1);
        for _ in 1..=n {
            p = q;
            q = r;
            r = p + q;
        }
        r
    }

    // 给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。
    pub fn delete_duplicates(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let mut head = head;
        let mut new_head = Some(Box::new(ListNode::new(0)));
        let mut prev_val: i32 = i32::min_value();
        let mut curr_node = &mut new_head;
        while let Some(node) = head.take() {
            if prev_val != node.val {
                if let Some(ref mut n) = curr_node {
                    n.next = Some(node.clone());
                    curr_node = &mut n.next;
                    prev_val = node.val;
                }
            }
            head = node.next;
        }
        if let Some(ref mut n) = curr_node {
            n.next = None;
        }
        new_head.unwrap().next
    }
}
