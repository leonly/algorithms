use std::cell::RefCell;
use std::rc::Rc;

// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

struct Solution;

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

impl Solution {
    // 给你一个二叉树，请你返回其按 层序遍历 得到的节点值。 （即逐层地，从左到右访问所有节点）。
    pub fn level_order(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<Vec<i32>> {
        let mut father: Vec<Rc<RefCell<TreeNode>>> = Vec::new();
        let mut children: Vec<Rc<RefCell<TreeNode>>> = Vec::new();
        if let Some(roo) = root {
            father.push(roo);
        }
        let mut re: Vec<Vec<i32>> = Vec::new();
        let mut one: Vec<i32> = Vec::new();
        while !father.is_empty() {
            let node = father.remove(0);
            one.push(node.borrow().val);
            if let Some(l) = node.clone().borrow().left.clone() {
                children.push(l);
            }
            if let Some(r) = node.clone().borrow().right.clone() {
                children.push(r);
            }
            if father.is_empty() {
                let tmp = father;
                father = children;
                children = tmp;
                re.push(one);
                one = Vec::new();
            }
        }
        re
    }
}
