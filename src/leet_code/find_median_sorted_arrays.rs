struct Solution;

impl Solution {
    pub fn find_median_sorted_arrays(nums1: Vec<i32>, nums2: Vec<i32>) -> f64 {
        let mut a = nums1.len();
        let mut b = nums2.len();
        let mut nums1 = nums1;
        let mut nums2 = nums2;
        if a > b {
            let tmp = nums1;
            nums1 = nums2;
            nums2 = tmp;
            a = nums1.len();
            b = nums2.len();
        }

        let mut mina: usize = 0;
        let mut maxa: usize = nums1.len();
        let half: usize = (a + b + 1) / 2;
        while mina <= maxa {
            let i = (mina + maxa) / 2;
            let j = half - i;

            if i > mina && nums1[i - 1] > nums2[j] {
                maxa = i - 1;
            } else if i < maxa && nums1[i] < nums2[j - 1] {
                mina = i + 1;
            } else {
                let max_left: f64;
                if i == 0 {
                    max_left = nums2[j - 1] as f64;
                } else if j == 0 {
                    max_left = nums1[i - 1] as f64;
                } else {
                    let a1: f64 = nums1[i - 1] as f64;
                    let b1: f64 = nums2[j - 1] as f64;
                    max_left = a1.max(b1);
                }
                if (a + b) % 2 == 1 {
                    return max_left;
                }
                let min_right: f64;
                if i == a {
                    min_right = nums2[j] as f64;
                } else if j == b {
                    min_right = nums1[i] as f64;
                } else {
                    let a1: f64 = nums1[i] as f64;
                    let b1: f64 = nums2[j] as f64;
                    min_right = a1.min(b1);
                }
                return (min_right + max_left) / 2.0;
            }
        }
        0.0
    }

    pub fn max_score(s: String) -> i32 {
        let mut sum1 = 0;
        for c in s.chars() {
            if c == '1' {
                sum1 += 1;
            }
        }
        let mut max_sc = 0;
        let mut num0 = 0;
        let mut num1 = 0;
        let mut k = 0;
        for c in s.chars() {
            k += 1;
            if c == '0' {
                num0 += 1;
            } else {
                num1 += 1;
            }
            let score = num0 + sum1 - num1;
            if score > max_sc && k < s.len() {
                max_sc = score;
            }
        }
        max_sc
    }

    pub fn max_score_1(card_points: Vec<i32>, k: i32) -> i32 {
        let k = k as usize;
        let (fk, _) = card_points.split_at(k);
        let (_, lk) = card_points.split_at(card_points.len() - k);
        let sum_fk: i32 = fk.iter().sum();
        let mut max_sc = sum_fk;
        let mut record = max_sc;
        for (i, item) in fk.iter().rev().enumerate() {
            record = record - item + lk[k - i - 1];
            if max_sc < record {
                max_sc = record;
            }
        }
        max_sc
    }
    pub fn find_diagonal_order(nums: Vec<Vec<i32>>) -> Vec<i32> {
        let mut re = Vec::new();
        let mut max_size: usize = nums.len();
        for (i, item) in nums.iter().enumerate() {
            if max_size < item.len() {
                max_size = item.len();
            }
            if i < nums.len() - 1 {
                let mut j = i as isize;
                let tmp = &nums[j as usize];
                while j >= 0 {
                    if tmp.len() > i - j as usize {
                        re.push(tmp[i - j as usize]);
                    }
                    j -= 1;
                }
            } else {
                for j in 0..max_size {
                    let mut k = i as isize;
                    let tmp = &nums[k as usize];
                    while k >= 0 {
                        let ind = i + j - k as usize;
                        if tmp.len() > ind {
                            re.push(tmp[ind]);
                        }
                        k -= 1;
                    }
                }
            }
        }
        re
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn faliliy() {
        let vec: Vec<Vec<i32>> = vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
        let re = Solution::find_diagonal_order(vec);
        assert_eq!(re, vec![1, 4, 2, 7, 5, 3, 8, 6, 9]);
    }
}
