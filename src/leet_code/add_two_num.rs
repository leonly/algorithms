#[derive(Debug, PartialEq, Eq, Clone)]
struct ListNode {
    val: i32,
    next: Option<Box<ListNode>>,
}

struct Solution {}

impl ListNode {
    fn new(val: i32) -> Self {
        ListNode {
            val: val,
            next: None,
        }
    }
}

impl Solution {
    pub fn add_two_numbers(
        l1: Option<Box<ListNode>>,
        l2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        let mut num1 = 0;
        let mut num2 = 0;
        let mut i = 0;
        let mut temp = l1;
        while temp.is_some() {
            let t1 = temp.unwrap();
            num1 = num1 + 10i32.pow(i) * t1.val;
            i += 1;
            temp = t1.next;
        }
        temp = l2;
        while temp.is_some() {
            let t2 = temp.unwrap();
            num2 = num2 + 10i32.pow(i) * t2.val;
            i += 1;
            temp = t2.next;
        }
        let mut num = num1 + num2;

        let mut result: Option<Box<ListNode>> = Some(Box::new(ListNode::new(num % 10)));
        let mut ptr = result.as_mut();
        while num / 10 >= 1 {
            num = num / 10;
            let mut a = ptr.unwrap();
            a.next = Some(Box::new(ListNode::new(num % 10)));
            ptr = a.next.as_mut();
        }
        result
    }
    pub fn add_two_numbers2(
        l1: Option<Box<ListNode>>,
        l2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        let mut t1 = l1;
        let mut t2 = l2;
        let mut plus = 0;
        let mut result: Option<Box<ListNode>> = Some(Box::new(ListNode::new(0)));
        let mut ptr = result.as_mut();
        let mut i = 0;

        while t1.is_some() || t2.is_some() {
            let mut sum = 0;
            if t1.is_some() {
                let tmp1 = t1.unwrap();
                sum += tmp1.val;
                t1 = tmp1.next;
            }
            if t2.is_some() {
                let tmp2 = t2.unwrap();
                sum += tmp2.val;
                t2 = tmp2.next;
            }
            sum += plus;
            if sum > 9 {
                plus = 1;
            } else {
                plus = 0;
            }
            let mut a = ptr.unwrap();
            if i > 0 {
                a.next = Some(Box::new(ListNode::new(sum % 10)));
                ptr = a.next.as_mut();
            } else {
                a.val = sum % 10;
                ptr = result.as_mut();
            }
            i += 1;
        }
        if plus == 1 {
            let mut a = ptr.unwrap();
            a.next = Some(Box::new(ListNode::new(1)));
        }
        result
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn add_two_num_test() {
        assert_eq!(1, 1);
    }
}
