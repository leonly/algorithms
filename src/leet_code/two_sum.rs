use std::collections::HashMap;

struct Solution {
    result: Vec<i32>,
}

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut result: Vec<i32> = Vec::new();
        let mut map: HashMap<i32, i32> = HashMap::new();
        for (i, num) in nums.into_iter().enumerate() {
            if map.contains_key(&(target - num)) {
                result.push(*map.get(&(target - num)).unwrap());
                result.push(i as i32);
                return result;
            }
            map.insert(num, i as i32);
        }
        result
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn tow_sum_test() {
        let a = vec![2, 7, 11, 15];
        let target = 9;
        assert_eq!(Solution::two_sum(a, target), vec![0, 1]);
    }
}
