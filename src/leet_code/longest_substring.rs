use std::cmp::max;
use std::collections::HashMap;

#[derive(Debug, PartialEq)]
struct Solution {}

impl Solution {
    pub fn length_of_longest_substring(s: String) -> i32 {
        let mut result = 0i32;
        let mut format_lsit: HashMap<char, i32> = HashMap::new();
        let mut tail = 0i32;
        let mut head = 0i32;
        let mut middle = 0i32;
        let mut last_repeat_location = 0i32;
        for (i, c) in s.chars().enumerate() {
            if format_lsit.contains_key(&c) {
                let location = *(format_lsit.get(&c).unwrap());
                if location >= last_repeat_location {
                    let l: i32 = i as i32 - location;
                    if l > result {
                        result = l;
                    }
                    if tail > middle {
                        middle = tail;
                    }
                    tail = l;
                    last_repeat_location = location;
                    format_lsit.insert(c, i as i32);
                    continue;
                }
            }
            if result < 1 {
                head += 1;
            } else {
                tail += 1;
            }

            format_lsit.insert(c, i as i32);
        }
        max(max(tail, middle), max(head, result))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn length_of_longest_substring_test() {
        assert_eq!(
            Solution::length_of_longest_substring("ohomm".to_string()),
            3i32
        );
    }
}
