use std::collections::BinaryHeap;
// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    pub fn new(val: i32, next: Option<Box<ListNode>>) -> Self {
        ListNode { next: next, val }
    }
    // next allow to change to linked node and to return the old one
    fn next(&mut self, next: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        std::mem::replace(&mut self.next, next)
    }

    fn elem(&self) -> &i32 {
        &self.val
    }
}

impl PartialOrd for ListNode {
    fn partial_cmp(&self, other: &ListNode) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for ListNode {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.val.cmp(&self.val)
    }
}

pub struct Solution;

impl Solution {
    pub fn merge_k_lists_p(lists: Vec<Option<Box<ListNode>>>) -> Option<Box<ListNode>> {
        if lists.is_empty() {
            return None;
        }
        let mut heap = BinaryHeap::with_capacity(lists.len());
        for li in lists {
            if let Some(node) = li {
                heap.push(node);
            }
        }

        let mut res = ListNode::new(0, None);
        let mut ptr = &mut res;

        while !heap.is_empty() {
            let mut min_node = heap.pop().unwrap();
            if heap.is_empty() {
                ptr.next = Some(min_node);
                break;
            }
            let next = min_node.next.take();
            if next.is_some() {
                heap.push(next.unwrap());
            }
            ptr.next = Some(min_node);
            ptr = ptr.next.as_deref_mut().unwrap();
        }
        res.next
    }
    pub fn merge_k_lists(lists: Vec<Option<Box<ListNode>>>) -> Option<Box<ListNode>> {
        let mut re: Option<Box<ListNode>> = None;
        for item in lists.into_iter() {
            re = Self::merge_2_lists(item, re, None);
        }
        re
    }

    fn merge_2_lists(
        a: Option<Box<ListNode>>,
        b: Option<Box<ListNode>>,
        accu: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        match (a, b) {
            (Some(mut a), Some(mut b)) => {
                if a.elem() < b.elem() {
                    Self::merge_2_lists(a.next(accu), Some(b), Some(a))
                } else {
                    Self::merge_2_lists(Some(a), b.next(accu), Some(b))
                }
            }
            (Some(a), None) => Self::rev(accu, Some(a)),
            (None, Some(b)) => Self::rev(accu, Some(b)),
            (None, None) => Self::rev(accu, None),
        }
    }

    // rev is needed when you deal with list
    fn rev(list: Option<Box<ListNode>>, accu: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        match list {
            Some(mut list) => Self::rev(list.next(accu), Some(list)),
            None => accu,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn merge_test() {
        let a = Some(Box::new(ListNode::new(
            21,
            Some(Box::new(ListNode::new(42, None))),
        )));

        let b = Some(Box::new(ListNode::new(
            1,
            Some(Box::new(ListNode::new(2, None))),
        )));
    }
}
