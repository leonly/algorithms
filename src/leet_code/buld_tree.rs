// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

struct Solution;

impl Solution {
    // 105. 从前序与中序遍历序列构造二叉树
    pub fn build_tree(preorder: Vec<i32>, inorder: Vec<i32>) -> Option<Rc<RefCell<TreeNode>>> {
        let pre_len = preorder.len();
        let in_len = inorder.len();
        assert_eq!(pre_len, in_len);
        if pre_len == 0 {
            return None;
        }
        let in_map: HashMap<&i32, usize> = inorder.iter().zip(0..in_len).collect();

        Solution::build_trees(&preorder, 0, pre_len - 1, &in_map, 0, in_len - 1)
    }

    fn build_trees(
        preorder: &Vec<i32>,
        pre_left: usize,
        pre_right: usize,
        in_map: &HashMap<&i32, usize>,
        in_left: usize,
        in_right: usize,
    ) -> Option<Rc<RefCell<TreeNode>>> {
        if pre_left > pre_right || in_left > in_right {
            return None;
        }
        let root_value = preorder[pre_left];
        let mut root_node = TreeNode::new(root_value);
        let pvot = in_map.get(&root_value).unwrap();
        root_node.left = Solution::build_trees(
            preorder,
            pre_left + 1,
            pvot - in_left + pre_left,
            in_map,
            in_left,
            pvot - 1,
        );
        root_node.right = Solution::build_trees(
            preorder,
            pvot - in_left + pre_left + 1,
            pre_right,
            in_map,
            pvot + 1,
            in_right,
        );

        Some(Rc::new(RefCell::new(root_node)))
    }
}
