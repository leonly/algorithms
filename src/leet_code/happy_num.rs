struct Solution;

impl Solution {
    pub fn is_happy(n: i32) -> bool {
        let mut slow = n;
        let mut fast = n;
        loop {
            slow = Self::bit_square_sum(slow);
            fast = Self::bit_square_sum(fast);
            fast = Self::bit_square_sum(fast);
            if slow == fast {
                break;
            }
        }
        slow == 1
    }

    fn bit_square_sum(n: i32) -> i32 {
        let mut sum = 0;
        let mut k = n;
        while k > 0 {
            let tem = k % 10;
            sum += tem.pow(2);
            k = k / 10;
        }
        sum
    }

    pub fn is_happy_i(n: i32) -> bool {
        let mut sum: i32 = 0;
        let mut k: i32 = n;
        let mut t = 0;

        while sum != 1 && t < 20 {
            sum = 0;
            while k > 0 {
                let tem = k % 10;
                sum += tem.pow(2);
                k = k / 10;
            }
            k = sum;
            t += 1;
        }
        if sum == 1 {
            true
        } else {
            false
        }
    }
}
