use std::mem::replace;

// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}
struct Solution;
impl Solution {
    // 给你一个链表，每 k 个节点一组进行翻转，请你返回翻转后的链表。
    // k 是一个正整数，它的值小于或等于链表的长度。
    // 如果节点总数不是 k 的整数倍，那么请将最后剩余的节点保持原有顺序。
    pub fn reverse_k_group(head: Option<Box<ListNode>>, k: i32) -> Option<Box<ListNode>> {
        let mut k_nodes: Vec<Box<ListNode>> = Vec::with_capacity(k as usize);
        let mut new_head = ListNode::new(0);
        let mut prev_last = &mut new_head.next;
        let mut next = head;
        while let Some(mut node) = next.take() {
            next = node.next.take();
            k_nodes.push(node);
            if k_nodes.len() == k as usize {
                while k_nodes.len() > 0 {
                    *prev_last = k_nodes.pop();
                    prev_last = &mut prev_last.as_mut().unwrap().next;
                }
            }
        }
        while k_nodes.len() > 0 {
            *prev_last = Some(k_nodes.remove(0));
            prev_last = &mut prev_last.as_mut().unwrap().next;
        }
        *prev_last = None;
        new_head.next
    }

    // 反转一个单链表。
    pub fn reverse_list(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let mut prev: Option<Box<ListNode>> = None;
        let mut curr: Option<Box<ListNode>> = head;
        while let Some(mut node) = curr.take() {
            curr = node.next;
            node.next = prev;
            prev = Some(node);
        }
        prev
    }

    // 反转一个单链表。
    pub fn reverse_list2(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        fn rev(list: Option<Box<ListNode>>, curr: Option<Box<ListNode>>) -> Option<Box<ListNode>>{
            match list {
                Some(mut list) => rev(replace(&mut list.next, curr), Some(list)),
                None  => curr,
            }
        }
        rev(head, None)
    }
}
