use std::cmp;
use std::cmp::{max, min};
use std::collections::HashMap;

pub struct Solution;

impl Solution {
    // 最长不重复子字符串
    pub fn length_of_longest_substring(s: String) -> i32 {
        let mut info: HashMap<char, usize> = HashMap::with_capacity(35);
        let mut split_index: usize = 0;
        let mut longest_num: i32 = 0;
        let mut tmp_num: i32 = 0;

        for (i, cha) in s.chars().enumerate() {
            if info.contains_key(&cha) {
                let v = info.get(&cha).unwrap();
                if *v >= split_index {
                    split_index = *v;
                    tmp_num = (i - split_index) as i32;
                } else {
                    tmp_num += 1;
                }
            } else {
                tmp_num += 1;
            }
            if tmp_num > longest_num {
                longest_num = tmp_num;
            }
            info.insert(cha, i);
        }
        longest_num
    }

    // 将一个给定字符串根据给定的行数，以从上往下、从左到右进行 Z 字形排列。
    pub fn convert(s: String, num_rows: i32) -> String {
        if num_rows == 1 {
            return s;
        }

        let mut res: Vec<String> = vec![String::new(); cmp::min(s.len(), num_rows as usize)];
        let mut cur_row: i32 = 0;
        let mut going_down = false;
        for c in s.chars() {
            res[cur_row as usize].push(c);
            if cur_row == 0 || cur_row == num_rows - 1 {
                going_down = !going_down;
            }
            cur_row += if going_down { 1 } else { -1 };
        }
        res.join("")
    }

    //请你来实现一个 atoi 函数，使其能将字符串转换成整数。
    pub fn my_atoi(st: String) -> i32 {
        let mut s = st.trim_start();
        let mut negtive = false;
        if s.starts_with('-') {
            negtive = true;
            let (_, k) = s.split_at(1);
            s = k;
        } else if s.starts_with('+') {
            let (_, k) = s.split_at(1);
            s = k;
        }
        let mut num = String::new();
        if negtive {
            num.push('-');
        }
        for c in s.chars() {
            if c >= '0' && c <= '9' {
                num.push(c);
            } else {
                break;
            }
        }
        match num.parse::<i32>() {
            Ok(n) => n,
            Err(e) => {
                if num.len() > 1 {
                    if negtive {
                        std::i32::MIN
                    } else {
                        std::i32::MAX
                    }
                } else {
                    0
                }
            }
        }
    }

    // 给定一个整数数组和一个整数 k，你需要找到该数组中和为 k 的连续的子数组的个数。
    /// 示例 1 :
    /// 输入:nums = [1,1,1], k = 2
    /// 输出: 2 , [1,1] 与 [1,1] 为两种不同的情况。
    ///
    /// 说明 :
    ///
    ///     数组的长度为 [1, 20,000]。
    ///     数组中元素的范围是 [-1000, 1000] ，且整数 k 的范围是 [-1e7, 1e7]。
    pub fn subarray_sum(nums: Vec<i32>, k: i32) -> i32 {
        let mut count: i32 = 0;
        let mut pre: i32 = 0;
        let mut mp: HashMap<i32, i32> = HashMap::with_capacity(nums.len());
        mp.insert(0, 1);
        for n in nums {
            pre += n;
            if mp.contains_key(&(pre - k)) {
                count += mp.get(&(pre - k)).unwrap();
            }
            mp.insert(pre, 1 + mp.get(&pre).unwrap_or(&0));
        }
        count
    }

    // 给你 n 个非负整数 a1，a2，...，an，每个数代表坐标中的一个点 (i, ai) 。在坐标内画 n 条垂直线，垂直线 i 的两个端点分别为 (i, ai) 和 (i, 0)。找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。
    // 说明：你不能倾斜容器，且 n 的值至少为 2。
    pub fn max_area(height: Vec<i32>) -> i32 {
        let mut result: i32 = 0;
        let mut left: usize = 0;
        let mut right = height.len() - 1;
        while left != right {
            if height[left] > height[right] {
                result = std::cmp::max(height[right] * ((right - left) as i32), result);
                right -= 1;
            } else {
                result = std::cmp::max(height[left] * ((right - left) as i32), result);
                left += 1;
            }
        }
        result
    }

    /// 罗马数字包含以下七种字符： I， V， X， L，C，D 和 M。
    /// 字符          数值
    /// I             1
    /// V             5
    /// X             10
    /// L             50
    /// C             100
    /// D             500
    /// M             1000
    ///
    /// 例如， 罗马数字 2 写做 II ，即为两个并列的 1。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。
    ///
    /// 通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 IIII，而是 IV。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况：
    ///
    ///     I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
    ///     X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。
    ///     C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
    ///
    /// 给定一个整数，将其转为罗马数字。输入确保在 1 到 3999 的范围内。
    pub fn int_to_roman(num: i32) -> String {
        let keys: Vec<i32> = vec![1, 5, 10, 50, 100, 500, 1000];
        let values: Vec<char> = vec!['I', 'V', 'X', 'L', 'C', 'D', 'M'];
        let mut spe: HashMap<i32, String> = HashMap::new();
        spe.insert(4, "IV".to_string());
        spe.insert(9, "IX".to_string());
        spe.insert(40, "XL".to_string());
        spe.insert(90, "XC".to_string());
        spe.insert(400, "CD".to_string());
        spe.insert(900, "CM".to_string());
        let mut result = String::new();
        let mut rest = num;
        for i in (0..keys.len()).rev() {
            let q = rest / keys[i];
            if q > 0 {
                let s;
                if rest > 100 && rest < 1000 {
                    s = rest / 100 * 100;
                } else if rest > 10 && rest < 100 {
                    s = rest / 10 * 10;
                } else if rest < 10 {
                    s = rest;
                } else {
                    s = q * keys[i];
                }

                if spe.contains_key(&s) {
                    result.push_str(spe.get(&s).unwrap());
                    rest -= s;
                } else {
                    for _ in 0..q {
                        result.push(values[i]);
                    }
                    rest -= q * keys[i];
                }
            } else {
                continue;
            }
        }
        result
    }

    // 13. 罗马数字转整数
    pub fn roman_to_int(s: String) -> i32 {
        let mut map: HashMap<char, i32> = HashMap::new();
        map.insert('I', 1);
        map.insert('V', 5);
        map.insert('X', 10);
        map.insert('L', 50);
        map.insert('C', 100);
        map.insert('D', 500);
        map.insert('M', 1000);

        let mut sum: i32 = 0;
        let mut last: i32 = 0;
        s.chars().for_each(|c| {
            let v = map.get(&c).unwrap();
            if v > &last {
                sum -= 2 * last;
            }
            last = *v;
            sum += *v;
        });
        sum
    }

    //  210 课程表 II
    // 邻接表+广度优先搜索
    pub fn find_order(num_courses: i32, prerequisites: Vec<Vec<i32>>) -> Vec<i32> {
        // 存储有向图
        let mut edges: Vec<Vec<i32>> = vec![vec![]; num_courses as usize];
        let mut indeg: Vec<i32> = vec![0; num_courses as usize];
        let mut result = Vec::with_capacity(num_courses as usize);

        for info in prerequisites {
            edges[info[1] as usize].push(info[0]);
            indeg[info[0] as usize] += 1;
        }

        let mut q: Vec<i32> = Vec::with_capacity(num_courses as usize);
        // 将所有入度为 0 的节点放入队列中
        for i in 0..num_courses {
            if indeg[i as usize] == 0 {
                q.push(i);
            }
        }
        while !q.is_empty() {
            let u: i32 = q.pop().unwrap();
            result.push(u);
            for &v in &edges[u as usize] {
                indeg[v as usize] -= 1;
                // 如果相邻节点 v 的入度为 0，就可以选 v 对应的课程了
                if indeg[v as usize] == 0 {
                    q.push(v);
                }
            }
        }
        if result.len() as i32 != num_courses {
            vec![]
        } else {
            result
        }
    }

    // 给你一个整数数组 nums ，请你找出数组中乘积最大的连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。
    pub fn max_product(nums: Vec<i32>) -> i32 {
        let mut result: i32 = nums[0];
        let mut min_r: i32 = nums[0];
        let mut max_r: i32 = nums[0];
        for &num in &nums[1..] {
            let mir = min_r;
            let mar = max_r;
            max_r = max(mar * num, max(num, mir * num));
            min_r = min(mir * num, min(num, mar * num));
            result = max(result, max_r);
        }
        result
    }

    // 1371. 每个元音包含偶数次的最长子字符串
    pub fn find_the_longest_substring(s: String) -> i32 {
        let (mut ans, mut status, mut n) = (0, 0, s.len());
        let mut pos = [-1; 32];
        pos[0] = 0;
        for (i, c) in s.char_indices() {
            match c {
                'a' => status ^= 1,
                'e' => status ^= 2,
                'i' => status ^= 4,
                'o' => status ^= 8,
                'u' => status ^= 16,
                _ => (),
            }
            match pos[status] {
                dt if dt >= 0 => ans = ans.max(i as i32 + 1 - dt),
                _ => pos[status] = i as i32 + 1,
            }
        }
        ans
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn find_order_test() {
        let num_courses: i32 = 4;
        let pres = vec![vec![1, 0], vec![2, 0], vec![3, 1], vec![3, 2]];
        assert_eq!(Solution::find_order(num_courses, pres), vec![0, 1, 2, 3]);
    }
}
