pub struct Solution;
impl Solution {
    pub fn longest_palindrome(s: String) -> String {
        if s.len() < 2 {
            return s;
        }
        let mut max_size = 1;
        let mut left: usize = 0;
        let mut right: usize = 0;
        let schars: Vec<char> = s.chars().collect();
        let mut same_num = 0;
        for i in 0..s.len() - 1 {
            if same_num > 0 {
                same_num -= 1;
                continue;
            }
            let mut size;

            size = 1;
            let mut m = i as isize - 1;
            let mut n = i + 1;
            let mut the_same = true;
            while n < s.len() {
                if m >= 0 && schars[m as usize] == schars[n] {
                    size += 2;
                    if schars[n] != schars[i] {
                        the_same = false;
                    }
                    m -= 1;
                    n += 1;
                } else {
                    if the_same && schars[n] == schars[i] {
                        size += 1;
                        n += 1;
                        same_num += 1;
                    } else {
                        break;
                    }
                }
            }
            if size > max_size {
                max_size = size;
                left = (m + 1) as usize;
                right = n - 1;
            }
        }
        s[left..=right].to_string()
    }
}
