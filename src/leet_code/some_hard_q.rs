struct Solution;

impl Solution {
    // 给你一个字符串 s 和一个字符规律 p，请你来实现一个支持 '.' 和 '*' 的正则表达式匹配。
    pub fn is_match(s: String, p: String) -> bool {
        if p.is_empty() {
            return s.is_empty();
        }
        let fist_match = (!s.is_empty()
            && (p.chars().nth(0).unwrap() == s.chars().nth(0).unwrap()
                || p.chars().nth(0).unwrap() == '.'));
        if p.len() >= 2 && p.chars().nth(1).unwrap() == '*' {
            Solution::is_match(String::from(&s), String::from(&p[2..]))
                || (fist_match && Solution::is_match(String::from(&s[1..]), p))
        } else {
            fist_match && Solution::is_match(String::from(&s[1..]), String::from(&p[1..]))
        }
    }

    // 335. 路径交叉
    pub fn is_self_crossing(x: Vec<i32>) -> bool {
        for i in 3..x.len() {
            if i >= 3 && x[i - 1] <= x[i - 3] && x[i] >= x[i - 2] {
                return true;
            }
            if i >= 4 && x[i - 3] == x[i - 1] && x[i] + x[i - 4] >= x[i - 2] {
                return true;
            }
            if i >= 5
                && x[i] + x[i - 4] >= x[i - 2]
                && x[i - 1] + x[i - 5] >= x[i - 3]
                && x[i - 2] > x[i - 4]
                && x[i - 3] > x[i - 1]
            {
                return true;
            }
        }
        false
    }

    // 301. 删除无效的括号
    pub fn remove_invalid_parentheses(s: String) -> Vec<String> {
        Vec::new()
    }
}
